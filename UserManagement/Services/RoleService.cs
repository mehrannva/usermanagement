﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserManagement.Data;
using UserManagement.Extensions.Kendo;
using UserManagement.Models.IdentityViewModels;

namespace UserManagement.Services
{
    public class RoleService : Controller, IRoleService
    {
        private readonly RoleManager<ApplicationRole> roleManager;
        private ApplicationDbContext _context;
        private ISession _session;
        private IHttpContextAccessor httpContext;
        public ISession Session { get { return _session; } }

        public RoleService(IHttpContextAccessor httpContextAccessor, RoleManager<ApplicationRole> roleManager, ApplicationDbContext context, IHttpContextAccessor httpContext)
        {
            _session = httpContextAccessor.HttpContext.Session;
            this.roleManager = roleManager;
            _context = context;
            this.httpContext = httpContext;
        }

        public IEnumerable<ApplicationRoleVM> Read()
        {
            return GetAll();
        }

        public IList<ApplicationRoleVM> GetAll()
        {
            using (var db = _context)
            {
                var result = Session.GetObjectFromJson<IList<ApplicationRoleVM>>("Roles");

                List<ApplicationRoleVM> model = new List<ApplicationRoleVM>();
                result = roleManager.Roles.Include(c => c.Users).Select(r => new
                {
                    RoleName = r.Name,
                    Id = r.Id,
                    Description = r.Description,
                    NumberOfUsers = r.Users.Count
                }).ToList()
                .Select(r => new ApplicationRoleVM
                {
                    RoleName = r.RoleName,
                    Id = r.Id,
                    Description = r.Description,
                    NumberOfUsers = (from u in db.Users
                                     join c in db.UserRoles on u.Id equals c.UserId
                                     join b in db.Roles on c.RoleId equals b.Id
                                     where b.Id == r.Id
                                     select r).ToList().Count()
                }).ToList();

                Session.SetObjectAsJson("Roles", result);

                return result;
            }
        }


        public async Task Create(ApplicationRoleVM role)
        {
            bool isExist = !String.IsNullOrEmpty(role.Id);
            ApplicationRole applicationRole = isExist ? await roleManager.FindByIdAsync(role.Id) :
            new ApplicationRole
            {
                CreatedDate = DateTime.UtcNow
            };
            applicationRole.Name = role.RoleName;
            applicationRole.Description = role.Description;
            applicationRole.IPAddress = httpContext.HttpContext.Connection.RemoteIpAddress.ToString();

            IdentityResult roleRuslt = isExist ? await roleManager.UpdateAsync(applicationRole)
                                                : await roleManager.CreateAsync(applicationRole);
            if (roleRuslt.Succeeded)
            {
                ViewBag.RoleState = "RoleAdded";
            }

            role.Id = applicationRole.Id;
        }

        public async Task Update(ApplicationRoleVM role)
        {
            bool isExist = !String.IsNullOrEmpty(role.Id);
            ApplicationRole applicationRole = isExist ? await roleManager.FindByIdAsync(role.Id) :
           new ApplicationRole
           {
               CreatedDate = DateTime.UtcNow
           };
            applicationRole.Name = role.RoleName;
            applicationRole.Description = role.Description;
            applicationRole.IPAddress = httpContext.HttpContext.Connection.RemoteIpAddress.ToString();
            IdentityResult roleRuslt = isExist ? await roleManager.UpdateAsync(applicationRole)
                                                : await roleManager.CreateAsync(applicationRole);
            if (roleRuslt.Succeeded)
            {
                ViewBag.RoleState = "RoleUpdated";
            }
        }

        public async Task Destroy(ApplicationRoleVM role)
        {
            if (!String.IsNullOrEmpty(role.Id))
            {
                ApplicationRole applicationRole = await roleManager.FindByIdAsync(role.Id);
                if (applicationRole != null)
                {
                    IdentityResult roleRuslt = roleManager.DeleteAsync(applicationRole).Result;
                    if (roleRuslt.Succeeded)
                    {
                        ViewBag.RoleState = "RoleDeleted";
                    }
                }
            }
        }
    }
}
