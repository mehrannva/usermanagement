﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserManagement.Data;
using UserManagement.Extensions.Kendo;
using UserManagement.Models.IdentityViewModels;

namespace UserManagement.Services
{
    public class UserService : Controller, IUserService
    {
        //private static bool UpdateDatabase = false;
        private Microsoft.AspNetCore.Http.ISession _session;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<ApplicationRole> roleManager;
        private ApplicationDbContext _context;

        public Microsoft.AspNetCore.Http.ISession Session { get { return _session; } }


        public UserService(IHttpContextAccessor httpContextAccessor, ApplicationDbContext context, UserManager<Data.ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            _session = httpContextAccessor.HttpContext.Session;
            this.roleManager = roleManager;
            this.userManager = userManager;
            _context = context;
        }

        public IEnumerable<UserVM> Read()
        {
            return GetAll();
        }

        public IList<UserVM> GetAll()
        {
            using (var db = _context)
            {
                var result = Session.GetObjectFromJson<IList<UserVM>>("Users");

                //if (result == null || UpdateDatabase)
                //{
                result = db.Users
                    .Include(c => c.Roles)
                    .ToList().Select(user =>
                    {
                        string existingRole = userManager.GetRolesAsync(user).Result.Single();
                        var role = roleManager.Roles.Single(r => r.Name == existingRole);

                        return new UserVM
                        {
                            Id = user.Id,
                            UserName = user.UserName,
                            Name = user.Name,
                            Email = user.Email,
                            ConfirmPassword = "---",
                            Password = "******",
                            Role = new RoleVM()
                            {
                                RoleId = role.Id,
                                RoleName = role.Name
                            }
                        };
                    }).ToList();

                Session.SetObjectAsJson("Users", result);
                //}
                return result;
            }
        }

        public async Task Create(UserVM user)
        {
            var entity = new ApplicationUser();
            entity.UserName = user.UserName;
            entity.Name = user.Name;
            entity.Email = user.Email;

            await RegisterUser(user, entity);

            user.Id = entity.Id;
        }

        public async Task RegisterUser(UserVM user, ApplicationUser entity)
        {
            IdentityResult result = await userManager.CreateAsync(entity, user.Password);
            if (result.Succeeded)
            {
                ApplicationRole applicationRole = await roleManager.FindByIdAsync(user.Role.RoleId);
                if (applicationRole != null)
                {
                    IdentityResult roleResult = await userManager.AddToRoleAsync(entity, user.Role.RoleName);
                    if (roleResult.Succeeded)
                    {
                        ViewBag.AddUserState = "Done";
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "خطا، لطفا مجددا تلاش نمائید");
                        ViewBag.AddUserState = "Faild";
                    }
                }
            }
        }

        public async Task Update(UserVM user)
        {
            var target = await userManager.FindByIdAsync(user.Id);
            if (target != null)
            {
                target.Name = user.Name;
                target.UserName = user.UserName;
                target.Email = user.Email;

                var result = await userManager.UpdateAsync(target);
                if (result.Succeeded)
                {
                    string existingRole = userManager.GetRolesAsync(target).Result.First();
                    string existingRoleId = roleManager.Roles.Single(r => r.Name == existingRole).Id;

                    var roleResult = await userManager.RemoveFromRoleAsync(target, existingRole);
                    if (roleResult.Succeeded)
                    {
                        var applicationRole = await roleManager.FindByIdAsync(user.Role.RoleId);
                        if (applicationRole != null)
                        {
                            var newRoleResult = await userManager.AddToRoleAsync(target, applicationRole.Name);
                            if (newRoleResult.Succeeded)
                                ViewBag.StatusMsg = "Ok";
                        }
                    }
                }

                await ChangePassword(user);
            }
        }

        public async Task ChangePassword(UserVM user)
        {
            var target = await userManager.FindByIdAsync(user.Id);

            if (user.Password != null && (user.Password == user.ConfirmPassword))
            {
                IdentityResult resultPass = await userManager.RemovePasswordAsync(target);
                if (resultPass.Succeeded)
                {
                    IdentityResult result = await userManager.AddPasswordAsync(target, user.ConfirmPassword);
                    if (result.Succeeded)
                        ViewBag.ResultPassword = "Ok";
                }
            }
        }

        public async Task Destroy(UserVM user)
        {
            ApplicationUser applicationUser = await userManager.FindByIdAsync(user.Id);

            if (applicationUser != null)
            {
                IdentityResult result = await userManager.DeleteAsync(applicationUser);
                if (result.Succeeded)
                    ViewBag.DeleteUserState = "Ok";
            }
        }

    }
}
