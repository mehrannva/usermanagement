﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserManagement.Models.IdentityViewModels;

namespace UserManagement.Services
{
    public interface IRoleService
    {
        IEnumerable<ApplicationRoleVM> Read();

        Task Create(ApplicationRoleVM role);

        Task Update(ApplicationRoleVM role);

        Task Destroy(ApplicationRoleVM role);
    }
}
