﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserManagement.Models.IdentityViewModels;

namespace UserManagement.Services
{
    public interface IUserService
    {
        IEnumerable<UserVM> Read();

        Task Create(UserVM user);

        Task Update(UserVM user);

        Task Destroy(UserVM user);
    }
}
