﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UserManagement.Models.IdentityViewModels
{
    public class ApplicationRoleVM
    {
        public string Id { get; set; }

        [Display(Name = "Role Name")]
        [Required]
        public string RoleName { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Count Of Users")]
        public int NumberOfUsers { get; set; }
    }
}
