﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserManagement.Models.IdentityViewModels
{
    public class RoleVM
    {
        public string RoleId { get; set; }

        public string RoleName { get; set; }
    }
}
