﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UserManagement.Data;
using Microsoft.AspNetCore.Identity;
using UserManagement.Services;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using UserManagement.Models.IdentityViewModels;

namespace UserManagement.Controllers
{
    public class ApplicationRoleController : Controller
    {
        private readonly RoleManager<ApplicationRole> roleManager;
        private IRoleService roleService;

        public ApplicationRoleController(RoleManager<ApplicationRole> roleManager, IRoleService roleService)
        {
            this.roleManager = roleManager;
            this.roleService = roleService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult RoleList([DataSourceRequest] DataSourceRequest request)
        {
            return Json(roleService.Read().ToDataSourceResult(request));
        }

        public async Task<ActionResult> CreateRole([DataSourceRequest] DataSourceRequest request,
        [Bind(Prefix = "models")]IEnumerable<ApplicationRoleVM> roles)
        {
            var results = new List<ApplicationRoleVM>();
            if (roles != null && ModelState.IsValid)
            {
                foreach (var role in roles)
                {
                    await roleService.Create(role);
                    results.Add(role);
                }
            }

            return Json(results.ToDataSourceResult(request, ModelState));
        }

        public async Task<ActionResult> EditRole([DataSourceRequest] DataSourceRequest request,
       [Bind(Prefix = "models")]IEnumerable<ApplicationRoleVM> roles)
        {
            var results = new List<ApplicationRoleVM>();
            if (roles != null && ModelState.IsValid)
            {
                foreach (var role in roles)
                {
                    await roleService.Update(role);
                }
            }

            return Json(results.ToDataSourceResult(request, ModelState));
        }

        public async Task<ActionResult> DeleteRole([DataSourceRequest] DataSourceRequest request,
       [Bind(Prefix = "models")]IEnumerable<ApplicationRoleVM> roles)
        {
            var results = new List<ApplicationRoleVM>();
            if (roles != null && ModelState.IsValid)
            {
                foreach (var role in roles)
                {
                    await roleService.Destroy(role);
                }
            }

            return Json(results.ToDataSourceResult(request, ModelState));
        }
    }
}