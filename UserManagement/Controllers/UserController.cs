﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UserManagement.Data;
using Microsoft.AspNetCore.Identity;
using UserManagement.Services;
using Microsoft.AspNetCore.Http;
using Kendo.Mvc.UI;
using UserManagement.Models.IdentityViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;
using Kendo.Mvc.Extensions;

namespace UserManagement.Controllers
{
    public class UserController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<ApplicationRole> roleManager;
        private ApplicationDbContext _context;
        private IUserService userService;
        private IHttpContextAccessor httpContext;

        public UserController(IHttpContextAccessor httpContext, UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager, ApplicationDbContext context, IHttpContextAccessor httpContextAccessor, IUserService userService)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            _context = context;
            this.userService = userService;
            this.httpContext = httpContext;
        }

        public IActionResult Index()
        {
            PopulateRoles();
            return View();
        }

        public IActionResult UserList([DataSourceRequest] DataSourceRequest request)
        {
            return Json(userService.Read().ToDataSourceResult(request));
        }

        public IActionResult RoleList([DataSourceRequest] DataSourceRequest request)
        {
            return Json(userService.Read().ToDataSourceResult(request));
        }

        [HttpPost]
        public async Task<ActionResult> CreateUser([DataSourceRequest] DataSourceRequest request,
            [Bind(Prefix = "models")]IEnumerable<UserVM> users)
        {
            var results = new List<UserVM>();
            if (users != null && ModelState.IsValid)
            {
                foreach (var user in users)
                {
                    await userService.Create(user);
                    results.Add(user);
                }
            }

            return Json(results.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs("Post")]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> EditUser([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<UserVM> users)
        {
            if (users != null && ModelState.IsValid)
            {
                foreach (var user in users)
                {
                    await userService.Update(user);
                }
            }

            return Json(users.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs("Post")]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> DeleteUser([DataSourceRequest] DataSourceRequest request,
          [Bind(Prefix = "models")]IEnumerable<UserVM> users)
        {
            foreach (var user in users)
            {
                await userService.Destroy(user);
            }

            return Json(users.ToDataSourceResult(request, ModelState));
        }

        private void PopulateRoles()
        {

            var roles = roleManager.Roles
                        .Select(c => new RoleVM
                        {
                            RoleId = c.Id,
                            RoleName = c.Name
                        })
                        .OrderBy(e => e.RoleName);

            if (roles.Count() > 0)
            {
                ViewData["roles"] = roles.ToList();
                ViewData["defaultRole"] = roles.First();
            }

        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult> UserProfile(string id)
        {
            if (User.Identity.IsAuthenticated == false)
                return RedirectToAction("AccessDenied", "Account");

            ViewData["Reffer"] = Request.Headers["Referer"].ToString();

            var model = new EditUserVM();
            model.ApplicationRoles = roleManager.Roles.Select(r => new SelectListItem
            {
                Text = r.Name,
                Value = r.Id
            }).ToList();

            ApplicationUser user = await userManager.FindByIdAsync(id);
            if (user != null)
            {
                model.Name = user.Name;
                model.UserName = user.UserName;
                model.Password = "***";
                model.Email = user.Email;
                model.ApplicationRoleId = roleManager.Roles.Single(r => r.Name == userManager.GetRolesAsync(user).Result.Single()).Name;
            }

            return View(model);
        }

    }
}